var fsUtil = require("./util/lw-fs");

// fsReadFileTest();
// fsReadFilePromiseTest();
// fsReaddirTest();
fsReadStreamTest();

// 异步读取文件和同步读取文件测试
function fsReadFileTest() {
    console.log("===============fsReadFileTest异步和同步读取文件测试=================")
    // 异步读取文件
    fsUtil.fsReadFile("files/read-file1.txt")

    fsUtil.fsRead("files/read-file1.txt")

    setTimeout(function () {
        console.log("。。。等待10毫秒。。。")
        // 同步读取文件
        let file2 = fsUtil.fsReadFileSync("files/read-file2.txt")
        console.log("同步读取文件[files/read-file2.txt]fsReadFileSync:" + file2);
    }, 10)

    // 同步读取文件
    let file3 = fsUtil.fsReadFileSync("files/read-file3.txt")
    console.log("同步读取文件[files/read-file3.txt]fsReadFileSync:" + file3);

    let file_original = fsUtil.fsReadSync("files/read-file3.txt")
    console.log("同步读取文件[files/read-file3.txt]fsReadFileSync_original:" + file_original);

}

// Promise异步读取文件测试 (Promise对象用来将异步操作以同步操作的流程表达出来)
async function fsReadFilePromiseTest() {
    console.log("===============fsReadFilePromiseTestPromise异步读取文件测试=================")
    let file1 = await fsUtil.fsReadFilePromise("files/read-file1.txt")
    console.log("Promise异步读取文件[files/read-file1.txt]，下一个文件:" + file1);
    let file2 = await fsUtil.fsReadFilePromise("files/"+file1+".txt")
    console.log("Promise异步读取文件[files/read-file2.txt]，下一个文件:" + file2);
    let file3 = await fsUtil.fsReadFilePromise("files/"+file2+".txt")
    console.log("Promise异步读取文件[files/read-file3.txt]，下一个文件:" + file3);
}

// 读取目录测试
function fsReaddirTest() {
    console.log("===============fsReaddirTest读取目录测试=================")
    fsUtil.fsReaddir("files");
    let dirItems = fsUtil.fsReaddirSync("files");
    console.log("同步读取目录[files]:" + dirItems);
}

// 读取流数据测试
function fsReadStreamTest() {
    console.log("===============fsReadStreamTest读取流数据测试=================")
    fsUtil.fsReadStream("files/stream-file.txt");
    fsUtil.fsReadStream("files/song.mp3");
}


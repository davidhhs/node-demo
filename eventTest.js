var fs = require("fs");
var fsUtil = require("./util/lw-fs");
var lwEvent = require("./util/lw-event").lwEvent;

myEventTest();
myEventPromiseTest();


// 自定义事件测试
function myEventTest() {
    console.log("===============myEventTest自定义事件测试=================")

    fs.readFile("files/read-file1.txt", {flag:"r",encoding:"utf-8"}, function (err, data) {
        if(err) {
            console.log("异步读取文件readFile: " + err)
        } else {
            console.log("异步读取文件readFile: " + data);
            // 自定义事件队列触发，代替原先的多个事件
            lwEvent.emit("myclick", "自定义参数")
            // 处理自定义事件1
            // 处理自定义事件2
            // 处理自定义事件3
        }
    })

    lwEvent.on("myclick", function (re) {
        console.log("处理自定义事件1。re: " + re)
    })
    lwEvent.on("myclick", function (re) {
        console.log("处理自定义事件2。re: " + re)
    })
    lwEvent.on("myclick", function (re) {
        console.log("处理自定义事件3。re: " + re)
    })

}

// 自定义事件监听异步方式测试
function myEventPromiseTest() {
    fsUtil.fsReadFilePromise("files/read-file1.txt").then(function(data) {
        // 依次触发自定义事件监听队列中的逻辑，不需要async await方式实现。也不用多个.then()
        lwEvent.emit("myclickPromise", "myclickPromise参数");
    });
    
    lwEvent.on("myclickPromise", function (re) {
        console.log("处理自定义事件myclickPromise1。re: " + re)
    })
    lwEvent.on("myclickPromise", function (re) {
        console.log("处理自定义事件myclickPromise2。re: " + re)
    })
    lwEvent.on("myclickPromise", function (re) {
        console.log("处理自定义事件myclickPromise3。re: " + re)
    })

}

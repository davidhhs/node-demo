var fsUtil = require("./util/lw-fs");

// fsWriteFileTest();
fsWriteStreamTest();


// 异步写文件测试
function fsWriteFileTest() {
    console.log("===============fsWriteFileTest异步写文件测试=================")
    // 同步写入文件
    fsUtil.fsWriteSync("files/write-file1.txt", "w")
    fsUtil.fsWriteSync("files/write-file1.txt", "a")

    // 同步覆盖写入文件
    fsUtil.fsWriteFileSync("files/write-file2.txt");

    // 同步追加写入文件
    fsUtil.fsAppendFileSync("files/write-file2.txt");
}

// 写入流数据测试
function fsWriteStreamTest() {
    console.log("===============fsWriteStreamTest写入流数据测试=================")
    fsUtil.fsWriteStream("files/song.mp3", "files/song-1.mp3");
    fsUtil.fsWriteStreamPipe("files/song.mp3", "files/song-2-pipe.mp3");
}


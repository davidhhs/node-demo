/**
 * 自定义事件工具类
 */

// 自定义事件对象（事件队列方式执行）
let lwEvent = {
    // 事件队列
    event: {},
    on: function (eventName, eventFn) {
        if (!this.event[eventName]) {
            this.event[eventName] = []
        }
        this.event[eventName].push(eventFn);
    },
    emit: function (eventName, eventMsg) {
        if (this.event[eventName]) {
            this.event[eventName].forEach(itemFn => {
                itemFn(eventMsg)
            })
        }
    }
}

/***
 * 输出模块函数
 */
exports.lwEvent = lwEvent;

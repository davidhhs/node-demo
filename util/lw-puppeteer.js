/**
 * Puppeteer 是一个 Node 库，它提供了一个高级 API 来通过 DevTools 协议控制 Chromium 或 Chrome。
 * Puppeteer API 是分层次的，反映了浏览器结构。
 */
let puppeteer = require("puppeteer")
let fsUtil = require("./lw-fs")

/**
 * Puppeteer打开浏览器
 */
async function openBrowserPage() {
    let options = {
        // 为每个页面设置一个默认视口大小。默认是 800x600。如果为 null 的话就禁用视图口。
        defaultViewport: {
            width: 1024,
            height: 768
        },
        // 是否以 无头模式 运行浏览器。默认是 true
        headless: false
    }
    // 开启浏览器
    let browser = await puppeteer.launch(options)
    // 打开新页签
    let page = await browser.newPage()
    // 打开网页
    await page.goto("https://www.dytt8.net/index.htm")
    // 创建目录
    fsUtil.fsMkdir("./files/download/browser");
    // 截屏
    page.screenshot({path: "./files/download/browser/screenshot.png"})
    // 获取页面内容
    page.$$eval("#menu li a", function(elements) {
        elements.forEach(function(v, i) {
            console.log(i, v.innerHTML)
        })
    })
}


/***
 * 输出模块函数
 */
exports.openBrowserPage = openBrowserPage;

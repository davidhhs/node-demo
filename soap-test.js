const soap = require('soap');
// import soap from 'soap';
let url = 'http://192.168.124.41:18080/webservice/userWebService?wsdl'
// let url = 'http://localhost:18080/webservice/userWebService?wsdl'
soap.createClient(url, function (err, client) {
  if (err) {
    console.error(err)
  } else {
    let xml = `<?xml version='1.0' encoding='UTF-8'?><USERMODIFYREQ><HEAD><CODE>0</CODE><SID>0</SID><TIMESTAMP>20240703191316</TIMESTAMP><SERVICEID>GSNGGIS</SERVICEID></HEAD><BODY><OPERATORID>admin</OPERATORID><OPERATORIP>127.0.0.1</OPERATORIP><MODIFYMODE>add</MODIFYMODE><USERINFO><USERID></USERID><LOGINNO>menglijuanGIS5</LOGINNO><USERNAME>孟丽娟</USERNAME><ORGID>10000</ORGID><EMAIL>18894022699@139.com</EMAIL><MOBILE>18894022699</MOBILE><PASSWORD>16|48|86|-121|109|-121|53|98|48|22|-49|50|-1|-5|86|-64|89</PASSWORD><STATUS>1</STATUS><EFFECTDATE>2024-07-03 19:12:57</EFFECTDATE><EXPIREDATE>2050-01-13 00:00:00</EXPIREDATE><REMARK></REMARK></USERINFO></BODY></USERMODIFYREQ>`
    let params = {
      RequestInfo: xml
    }
    params = xml
    client.UpdateAppAcctSoap(params, function (err, result) {
      if (err) {
        console.error("错误信息：", err)
      } else {
        console.log("成功信息：", result)
      }
    })
  }
})

async function pipe() {
	var url = "http://localhost:18080/webservice/userWebService";
	var xml = "<?xml version='1.0' encoding='UTF-8'?><USERMODIFYREQ><HEAD><CODE>0</CODE><SID>0</SID><TIMESTAMP>20240703191316</TIMESTAMP><SERVICEID>GSNGGIS</SERVICEID></HEAD><BODY><OPERATORID>admin</OPERATORID><OPERATORIP>127.0.0.1</OPERATORIP><MODIFYMODE>add</MODIFYMODE><USERINFO><USERID></USERID><LOGINNO>menglijuanGIS5</LOGINNO><USERNAME>孟丽娟</USERNAME><ORGID>10000</ORGID><EMAIL>18894022699@139.com</EMAIL><MOBILE>18894022699</MOBILE><PASSWORD>16|48|86|-121|109|-121|53|98|48|22|-49|50|-1|-5|86|-64|89</PASSWORD><STATUS>1</STATUS><EFFECTDATE>2024-07-03 19:12:57</EFFECTDATE><EXPIREDATE>2050-01-13 00:00:00</EXPIREDATE><REMARK></REMARK></USERINFO></BODY></USERMODIFYREQ>";
	var param = {
		RequestInfo: xml
	};
	param = xml;
	var wsdlOptions = {
		// "returnMethodSign": "ns1:responseInfo",
		// "SOAPAction": ""
	}; //非特殊情况下使用不到该配置
	var serverName = "UpdateAppAcctSoap";
	var serverNameReturn = serverName + "Return";
	log("******调用应用从帐号变更接口url:{}", url);
	log("******调用应用从帐号变更接口serverName:{}", serverName);
	log("******调用应用从帐号变更接口param:{}", param);

	var result = await soap(url, serverName, param, wsdlOptions);
	log("******调用应用从帐号变更接口result:{}", result);
	var ret = result[serverNameReturn].$value;
	log("******调用应用从帐号变更接口返回数据:{}", ret);

}
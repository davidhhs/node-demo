
var fs = require("fs");
var $ = require("jquery");


//writeTest();
test();

function test() {
  console.log($);

}


// 文件写入测试
function writeTest() {

  var fd;
  try {
    fd = fs.openSync("files/index-test.txt", "w");
    fs.writeSync(fd, "文件内容被写成这句话了_" + new Date().toLocaleDateString());
    console.log("writeSync() 文件内容被写成这句话了");
  } catch (err) {
    //console.log(arguments);
    console.log("writeSync() 文件内容被写成这句话了" + err);
  } finally {
    if (fd !== undefined)
      fs.closeSync(fd);
    console.log("writeSync() 文件写入内容，并关闭文件流。。。");
  }


  try {
    fs.appendFileSync("files/index-test.txt", "\n追加的数据_" + new Date().toLocaleDateString());
    console.log("appendFileSync() 数据已被追加到文件");
  } catch (err) {
    console.log("appendFileSync() 数据已被追加到文件" + err);
  } finally {
    console.log("appendFileSync() 文件写入内容，并关闭文件流。。。");
  }

  try {
    fd = fs.openSync("files/index-test.txt", "a");
    fs.writeSync(fd, "\n文件内容被追加了这句话_" + new Date().toLocaleDateString());
    console.log("writeSync() 文件内容被追加了这句话");
  } catch (err) {
    console.log("writeSync() 文件内容被追加了这句话" + err);
  } finally {
    if (fd !== undefined)
      fs.closeSync(fd);
    console.log("writeSync() 文件追加写入内容，并关闭文件流。。。");
  }

}


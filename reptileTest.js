
var reptile = require("./util/lw-reptile.js");

// reptileMoviesTest();
// reptileImgTest()
// requestWithProxyTest()
getMusicPageListTest()

// 爬电影测试
function reptileMoviesTest() {
    console.log("===============reptileMoviesTest爬虫爬电影测试=================")

    reptile.getMovieClassUrls();
    // reptile.getMovieUrlsByClass("https://vip.1905.com/list/t_1/p1o6.shtml");
    // reptile.getMovieInfoByUrl("https://vip.1905.com/play/1517177.shtml");

}

// 爬表情图片测试
function reptileImgTest() {
    console.log("===============reptileImgTest爬虫爬表情图片测试=================")

    reptile.getImgPageList();
    // reptile.parseImgPageInfo("https://www.doutula.com/article/detail/2287979");

}

// 使用代理发送请求测试
function requestWithProxyTest() {
    console.log("===============requestWithProxyTest使用代理发送请求测试=================")

    reptile.requestWithProxy();

}

// 获取歌曲分页信息并下载测试
function getMusicPageListTest() {
    console.log("===============getMusicPageListTest获取歌曲分页信息并下载测试=================")

    reptile.getMusicPageList();

}
